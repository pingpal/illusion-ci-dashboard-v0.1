var AppDispatcher = require('./AppDispatcher')

var SessionStateActions = {
    update: function(state) {
        AppDispatcher.handleViewAction({
            actionType: 'session-state-update',
            state: state
        })
    },
    destroy: function() {
        AppDispatcher.handleViewAction({
            actionType: 'session-state-destroy'
        })
    },
}

module.exports = SessionStateActions
