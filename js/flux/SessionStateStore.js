var AppDispatcher = require('./AppDispatcher')
var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var _state = { id: -1, mail: null, groups: [] }, _null = _state

function update(state) {
    _state = state
}

function destroy() {
    _state = _null
}

var SessionStateStore = assign({}, EventEmitter.prototype, {
    get: function() {
        return _state
    },
    emitChange: function() {
        this.emit('change')
    },
    addChangeListener: function(callback) {
        this.on('change', callback)
    },
    removeChangeListener: function(callback) {
        this.removeListener('change', callback)
    },
    dispatcherIndex: AppDispatcher.register(function(payload) {
        switch(payload.action.actionType) {
            case 'session-state-update' :
                update(payload.action.state)
                SessionStateStore.emitChange()
                break

            case 'session-state-destroy' :
                destroy()
                SessionStateStore.emitChange()
                break
        }

        return true
    })
})

module.exports = SessionStateStore
