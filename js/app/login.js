var $ = require('jquery')

var net = require('./net')

var React = require('react')
var Router = require('react-router')

var Whatevs = require('react-power-pack/whatevs')
var Form = require('react-power-pack/login')

var m = require('react-power-pack/merge')

var Link = Router.Link

var styles = {}

styles.logo = {
    margin: 0,
    padding: '15px 0',
    textAlign: 'center',
    fontFamily: "'Patua One', cursive",
    color: '#079DD9',
    fontWeight: 'normal'
}

styles.info = {
    margin: 0,
    padding: '15px 0',
    textAlign: 'center',
    fontFamily: "'Patua One', cursive",
    color: '#079DD9',
    fontWeight: 'normal'
}

styles.bg = {
    height: '100%',

    display: 'flex',
    display: '-webkit-flex',
    flexDirection: 'column',
    WebkitFlexDirection: 'column',
    alignItems: 'center',
    WebkitAlignItems: 'center',
    justifyContent: 'center',
    WebkitJustifyContent: 'center',

    background: '#ecf0f1'
}

styles.termsAndConditions = {
    height: 20,
    marginBottom: -20,
    fontSize: 11,
    color: '#333',
    textAlign: 'center'
}

styles.button = {
    position: 'absolute',
    top: 39,
    left: 20,

    padding: '1px 5px 1px 2px',
    borderRadius: '0 2px 2px 0',
}

styles.btnDepth = {
    display: 'inline-block',

    marginBottom: 0,
    padding: '1px 5px 1px 5px',

    fontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
    color: 'rgb(255, 255, 255)',
    fontSize: 12,
    fontWeight: 900,
    lineHeight: '18px',
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',

    borderRadius: 2,
    background: '#079DD9',
    boxShadow: '#497591 0px 2px 0px 0px'
}

styles.arrow = {
    position: 'absolute',
    top: 0,
    left: -20,
    width: 1,
    height: 1,
    border:  '10px solid transparent',
    borderRightColor: '#079DD9'
}

styles.arrowShadow = {
    top: 2,
    borderRightColor: '#497591'
}

styles.arrowSupport = {
    top: 1,
}

styles.alert = {
    width: 360,
    height: 100,
    marginBottom: -100,
    paddingTop: 20
}

styles.alertDanger = {
    paddingTop: 30
}

var Login = React.createClass({
    componentWillMount: function () {
        setTimeout(function () { $('body > .loader').addClass('done') }, 2000)
        setTimeout(function () { $('body > .loader').hide() }, 2500)
        setTimeout(function () { $('body > .loader').hide() }, 0)
    },
    getInitialState: function () {
        return { error: null, whatevs: this.props.whatevs || new Whatevs() }
    },
    handleSignIn: function (username, password) {

        if (username && password) {

            var data = { username: username, password: password }

            net.request('post!api/mail.signin', data, function (data) {
                if (data.result != 'success') {
                    this.state.whatevs.exe('login:shake')
                } else {
                    net.possibleSessionStateChanged()
                }
            }.bind(this))

            return true
        }

        this.state.whatevs.exe('login:shake')
    },
    handleSignUp: function (username, password) {
        this.setState({ error: null })

        if (username && password) {

            var data = { username: username, password: password }

            net.request('post!api/mail.signup?redirect=/', data, function (data) {
                if (data.result != 'success') {
                    this.setState({ error: data.response })

                } else {
                    net.possibleSessionStateChanged()
                }
            }.bind(this))

            return true
        }

        this.state.whatevs.exe('login:shake')
    },
    handleForgot: function (username, password) {

        if (username && password) {

            var data = { username: username, password: password }

            net.request('post!api/mail.reset?redirect=/', data, function (data) {
                if (data.result != 'success') {
                    this.setState({ error: data.response })

                } else {

                }
            }.bind(this))

            return true
        }

        this.state.whatevs.exe('login:shake')

        this.state.whatevs.exe('login:shake')
    },
    handleGotoSignin: function () {
        this.setState({ error: null, current: 'signin' })
    },
    handleGotoSignup: function () {
        this.setState({ current: 'signup' })
    },
    handleGotoForgot: function () {
        this.setState({ current: 'forgot' })
    },
    handleTermsAndConditions: function () {
        window.open('http://google.com','_blank')
    },
    renderSignin: function () {
        return (
            <div>
                <h1 style={styles.logo}>ci</h1>
                <Form text="Sign in" btnUser="Sign up!" btnPass="Forgot?" handleLogin={this.handleSignIn} onBtnUserClick={this.handleGotoSignup} onBtnPassClick={this.handleGotoForgot} whatevs={this.state.whatevs.ns('login')}/>
            </div>
        )
    },
    renderSignup: function () {
        return (
            <div style={{ position: 'relative' }}>
                <a href="#" tabIndex="-1" style={m(styles.btnDepth, styles.button)} onClick={this.handleGotoSignin}>
                    <span style={m(styles.arrow, styles.arrowShadow, styles.arrowSupport)}></span>
                    <span style={m(styles.arrow, styles.arrowShadow)}></span>
                    <span style={styles.arrow}></span>
                    Back
                </a>

                <h1 style={styles.info}>sign up</h1>
                <Form text="Sign up" handleLogin={this.handleSignUp} onBtnUserClick={this.handleGotoSignin} onInputFocus={ function () { this.setState({ error: null }) }.bind(this) } whatevs={this.state.whatevs.ns('login')}/>
                <p style={styles.termsAndConditions}>By signing up you agree to the terms and conditions found <a href="#" onClick={this.handleTermsAndConditions}>here</a>.</p>
            {this.state.error&&
                <div style={m(styles.alert, styles.alertDanger)}>
                    <div className="alert alert-danger">
                        <button type="button" className="close" onClick={ function () { this.setState({ error: null }) }.bind(this) }>×</button>
                        {this.state.error}
                    </div>
                </div>
            }
            </div>
        )
    },
    renderForgot: function () {
        return (
            <div style={{ position: 'relative' }}>
                <a href="#" tabIndex="-1" style={m(styles.btnDepth, styles.button)} onClick={this.handleGotoSignin}>
                    <span style={m(styles.arrow, styles.arrowShadow, styles.arrowSupport)}></span>
                    <span style={m(styles.arrow, styles.arrowShadow)}></span>
                    <span style={styles.arrow}></span>
                    Back
                </a>

                <h1 style={styles.info}>lost pass</h1>
                <Form handleLogin={this.handleForgot} whatevs={this.state.whatevs.ns('login')}/>
                <div style={styles.alert}>
                    <div className="alert alert-warning" >
                        <strong>Choose a new password</strong> and we&#39;ll send you a mail<br />with a link, please click it to verify your identitiy.
                    </div>
                </div>
            </div>
        )
    },
    render: function () {
        var current

        switch (this.state.current) {
            case 'forgot':
                current = this.renderForgot()
                break;
            case 'signup':
                current = this.renderSignup()
                break;
            default:
                current = this.renderSignin()
        }

        return (
            <div style={styles.bg}>
                <div style={{position: 'relative', top: -100}}>
                    {current}
                </div>
            </div>
        )
    }
})

module.exports = Login
