/**
 * Boilerplate code.
 */

var $ = require('jquery')
var React = require('react')
var Router = require('react-router')

var net = require('./net')

var RouteHandler = Router.RouteHandler
var Link = Router.Link

var styles = {}

styles.logo = {
    position: 'absolute',
    top: 0,
    left: 250,
    right: 0,
    margin: 0,
    padding: '15px 0',
    height: 90,
    background: 'white',
    textAlign: 'center',
    fontFamily: "'Patua One', cursive",
    color: '#58ade3',
    fontWeight: 'normal'
}

styles.bg = {
    height: '100%',
    margin: '0 0 0 250px',
    padding: '90px 0 0 0',
    background: 'whitesmoke'
}

styles.top = {
    height: 90,
    background: '#F5F6F7',
    //background: '#F3F4F6',
}

styles.nav = {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    width: 250,
    background: '#E6E9ED'
}

styles.ul = {
    margin: 0,
    padding: '50px 20px 0 20px',
    listStyleType: 'none'
}

styles.li = {
    color: 'white',
    fontFamily: "'PT Sans', sans-serif",
    fontSize: 22
}

var Nav = React.createClass({
    handleLogout: function (e) {
        e.preventDefault()

        net.request('api/mail.signout', function (data) {
            net.possibleSessionStateChanged()
        }.bind(this))
    },
    render: function () {
        var menu = [
            [ 'dashboard', 'Dashboard', 'fa-cog' ],
            [ 'download', 'Download', 'fa-cog' ],
            [ 'build', 'Build', 'fa-cog' ],
            [ 'settings', 'Settings', 'fa-cog' ],
        ]

        var list = menu.map(function (item, index) {
            return (
                <li key={index} style={styles.li}>
                    <Link to={item[0]} >{item[1]}</Link>
                </li>
            )
        })

        return (
            <div style={styles.nav}>
                <div style={styles.top}></div>
                <ul style={styles.ul}>{list}</ul>
                <div style={{ padding: 20 }}>
                    <a href="#" className="btn btn-warning" onClick={this.handleLogout}>Logout</a>
                </div>
            </div>
        )
    }
})

var App = React.createClass({
    componentWillMount: function () {
        // Remove the loader screen, wait some time for effect
        setTimeout(function () { $('body > .loader').addClass('done') }, 2000)
        setTimeout(function () { $('body > .loader').hide() }, 2500)
        setTimeout(function () { $('body > .loader').hide() }, 0)
    },
    render: function () {
        return (
            <div style={styles.bg}>
                <Nav />
                <h1 style={styles.logo}>CI</h1>
                <RouteHandler/>
            </div>
        )
    }
})

module.exports = App
