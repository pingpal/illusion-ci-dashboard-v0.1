var $ = require('jquery')
var React = require('react')

var net = require('./net')

var styles = {}

styles.content = {
    padding: '0 50px'
}

var Download = React.createClass({
    getInitialState: function () {
        return { current: null, oldies: [], projects: [] }
    },
    componentWillMount: function () {
        net.req('/api/ci.projects', function (data) {
            this.setState({ projects: data.response })
        }.bind(this))
    },
    handleOlderVersions: function (project, e) {
        e.preventDefault()

        if (project == this.state.current) {
            this.setState({ current: null })

        } else {
            net.req('/api/ci.get?project=' + project, function (data) {
                this.setState({ current: project, oldies: data.response })
            }.bind(this))
        }
    },
    render: function () {
        var oldies = this.state.oldies.map(function (oldie) {
            return <div key={oldie.id}><a href={"/api/ci.download?id=" + oldie.id} >{this.state.current + ' ' + oldie.name}</a></div>
        }.bind(this))

        var projects = this.state.projects.map(function (project) {
            return (
                <div key={project}>
                    <h3>{project}</h3>
                    <a href={'/api/ci.download?project=' + project } className="btn btn-lg btn-primary">Download latest version</a>
                    <br />
                    <br />
                    <a href="#" className="btn btn-default" onClick={this.handleOlderVersions.bind(this, project)}>Older versions</a>
                    { project == this.state.current && <div ref={project} style={{ paddingTop: 20 }}>{oldies}</div> }
                </div>
            )
        }.bind(this))

        return (
            <div style={styles.content}>
                <h1>Download</h1>
                {projects}
            </div>
        )
    }
})

module.exports = Download
