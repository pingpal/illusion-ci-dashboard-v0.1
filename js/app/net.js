var $ = require('jquery')
var Whatevs = require('react-power-pack/whatevs')

var Net = function () {
    this.in = null
    this.we = new Whatevs()
}

Net.prototype.onSessionStateIn = function (that, cb) {
    return this.we.on('in', that, cb)
}

Net.prototype.onSessionStateOut = function (that, cb) {
    return this.we.on('out', that, cb)
}

Net.prototype.onSessionStateChange = function (that, cb) {
    return this.we.on('change', that, cb)
}

Net.prototype.off = function (t, c) {
    return this.we.off(null, t, c)
}

Net.prototype.possibleSessionStateChanged = function (callback) {
    this.request('api/mail.info', function (data) {
        this.we.exe('change', data)
        if (data.result != 'success') {
            console.log('lucky i did')
            if (this.in !== false) {
                this.in = false
                this.we.exe('out', data)
            }

        } else {
            callback && callback(data)

            if (this.in !== true) {
                this.in = true
                this.we.exe('in', data)
            }
        }
    }.bind(this))
}

Net.prototype.req = function(url, data, callback) {
    if ($.isFunction(data)) {
		callback = data
		data = undefined
	}

    this.request(url, data, function (data) {
        if (data.result != 'success'/* && data.code == 2*/) {
            console.log('better call saul')
            this.possibleSessionStateChanged(function () {
                callback && callback(data)
            })

        } else {
            callback && callback(data)
        }
    }.bind(this))
}

Net.prototype.request = function(url, data, callback) {
	if ($.isFunction(data)) {
		callback = data
		data = undefined
	}

    var found = url.match(/^([^:]*)(?::([^:]+))?!/) || [ 0, 0, 0 ]
    var type = found[2], method = found[1] || 'get'
    var u = url.replace(/^[a-zA-Z:]*!/, '')

	type = type || ((u.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i) || [0, location.host])[1] === location.host ? 'json' : 'jsonp')

	$.ajax({
		url : u, data : data, dataType : type, type : method, timeout: 1000 * 60 * 10,

		success: function(object) {
			callback && callback(object)
		},
		error : function(x) {
			callback && callback(type == 'text' ? x.responseText || '' : x.responseJSON || {})
		}
	})
}


module.exports = new Net()
