var React = require('react')

var SessionStateStore = require('../flux/SessionStateStore')

var styles = {}

styles.content = {
    padding: '0 50px'
}

var Board = React.createClass({
    _onChange: function() {
        this.setState({ sessionState: SessionStateStore.get() })
    },
    getInitialState: function() {
        return { sessionState: SessionStateStore.get() }
    },
    componentDidMount: function() {
        SessionStateStore.addChangeListener(this._onChange)
    },
    componentWillUnmount: function() {
        SessionStateStore.removeChangeListener(this._onChange)
    },
    render: function () {
        if (~this.state.sessionState.groups.indexOf('super')) {

        } else if (!~this.state.sessionState.groups.indexOf('mailverified')) {
            return <h1>hi unknown</h1>
        }

        return (
            <div style={styles.content}>
                <h1>Dashboard</h1>
            </div>
        )
    }
})

module.exports = Board
