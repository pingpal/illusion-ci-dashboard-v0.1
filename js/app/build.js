var $ = require('jquery')
var React = require('react')

var net = require('./net')

var m = require('react-power-pack/merge')
var find = require('react-power-pack/find')
var Whatevs = require('react-power-pack/whatevs')
var Typein = require('react-power-pack/typein')
var Benjamin = require('react-power-pack/benjamin')

var styles = {}

styles.content = {
    padding: '0 50px'
}

styles.box = {

}

styles.row = {
    height: 60,

    paddingTop: 8,

    display: '-webkit-flex',
    display: 'flex',

    WebkitFlexDirection: 'row',
    flexDirection: 'row'
}

styles.col = {

    WebkitFlex: '2 0 0',
    flex: '2 0 0',

    width: '50px',
    height: 52,
    padding: 15,

    color: '#333',

    borderBottom: 'solid 1px #eee',
    background: 'white'
}

styles.colSm = {
    WebkitFlex: '1 0 0',
    flex: '1 0 0',
    textAlign: 'right'
}

var Build = React.createClass({
    getInitialState: function () {
        return { items: [], next: null, prev: null, whatevs: this.props.whatevs || new Whatevs() }
    },
    componentDidMount: function () {
        this.doRefresh()
    },
    componentDidUpdate: function () {
        $(find(this)).find('[data-its=build]').btnWorkIndctr({ delay: 100, spinner: { scale: .7 } })
    },
    doRefresh: function (crud, e) {
        e && e.preventDefault()

        crud || (crud = 'read(cipjs).get(id, repo).range(5)')

        net.req('/crud/' + crud, function (data) {
            if (data.result == 'success') {
                this.setState({
                    items: data.response.items,
                    next: data.response.next,
                    prev: data.response.prev
                })
            }

        }.bind(this))
    },
    doAdd: function (e) {
        e && e.preventDefault()

        var repo = this.state.whatevs.exe('repo:grab-typed')
        this.state.whatevs.exe('repo:clear-typed')

        net.req('/api/ci.add', { repo: repo }, function () {
            this.doRefresh()
        }.bind(this))
    },
    doRemove: function (id, e) {
        e.preventDefault()

        net.req('/crud/delete(cipjs).filter(id = ' + id + ')', function () {
            this.doRefresh()
        }.bind(this))
    },
    doBuild: function (id, e) {
        e.preventDefault()

        var $el = $(e.target)

        $el.btnWorkIndctr('resume')

        net.req('/api/ci.build', { id: id }, function (data) {
            $el.btnWorkIndctr('resign')
            if (confirm(data.result + '! Display result?')) {
                alert(data.response.join("\n"))
            }
        }.bind(this))
    },
    render: function () {

        var items = this.state.items.map(function (item) {
            return (
                <div key={item.id} style={m(styles.row)}>
                <div style={m(styles.col)}>{item.repo}</div>
                    <div style={m(styles.col, styles.colSm)}>
                        <a href="#" className="btn btn-primary btn-xs btn-depth-primary" onClick={this.doBuild.bind(this, item.id)} data-its="build">Build</a>
                    </div>
                    <div style={m(styles.col, styles.colSm)}>
                        <a href="#" className="btn btn-primary btn-xs btn-depth-primary" onClick={this.doRemove.bind(this, item.id)}>Remove</a>
                    </div>
                </div>
            )
        }.bind(this))

        items.length || (items =
            <div>No items</div>
        )

        var prev = this.state.prev ?
            <a href="#" className="btn btn-warning pull-right" onClick={this.doRefresh.bind(this, this.state.prev)}>Previous</a> :
            <a href="#" className="btn btn-default pull-right disabled" style={{ color: '#555' }}>Previous</a>

        var next = this.state.next ?
            <a href="#" className="btn btn-warning pull-right" onClick={this.doRefresh.bind(this, this.state.next)}>Next</a> :
            <a href="#" className="btn btn-default pull-right disabled" style={{ color: '#555' }}>Next</a>

        return (
            <div style={styles.content}>
                <h1>Build</h1>
                <br />
                <div>
                    <div className="row">
                        <div className="col-xs-10">
                            <Typein hint="Repo..." whatevs={this.state.whatevs.ns('repo')} onSubmit={this.doAdd}/>
                        </div>
                        <div className="col-xs-2">
                            <Benjamin text="Add" onClick={this.doAdd} />
                        </div>
                    </div>
                </div>
                <br />
                <div style={styles.box}>{items}</div>
                <br />
                {next}
                <span className="pull-right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                {prev}
            </div>
        )
    }
})

module.exports = Build
