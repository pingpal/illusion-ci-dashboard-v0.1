# Illusion CI dashboard

## Install

1. git clone git@bitbucket.org:pingpal/illusion-ci-dashboard-v0.1.git illusion-ci-dashboard
2. cd illusion-ci-dashboard
3. npm install
4. gulp

### Run web server e.g. illusion webif

1. cd ..
2. php -r "eval(file_get_contents('http://illusion.jonashall.in/get'));"
3. illusion check - fix mysql
4. cd illusion
5. curl -sS https://getcomposer.org/installer | php
6. illusion add git@bitbucket.org:pingpal/illusion-ci-backend-v0.1.git
7. illusion setup
8. illusion sw.setup
9. cd ../illusion-ci-dashboard
10. illusion webif --www=$(PWD)

### AUTHORS
Jonas Hallin

### License

Copyright (c) 2011 Jonas Hallin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.