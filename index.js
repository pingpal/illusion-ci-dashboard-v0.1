/**
 * Bootstrap. Set up routes.
 */

var $ = require('jquery')

var net = require('./js/app/net')

var React = require('react')
var Router = require('react-router')

var App = require('./js/app/app')
var Login = require('./js/app/login')
var Board = require('./js/app/board')
var Build = require('./js/app/build')
var Download = require('./js/app/download')

var SessionStateActions = require('./js/flux/SessionStateActions')

var RouteHandler = Router.RouteHandler
var DefaultRoute = Router.DefaultRoute
var Route = Router.Route
var Link = Router.Link

require('btn-work-indctr')

var Root = React.createClass({
    render: function () {
        return <RouteHandler />
    }
})

var Settings = React.createClass({
    render: function () {
        return <h1>Settings</h1>
    }
})

var routes = (
    <Route name="root" path="/" handler={Root}>
        <DefaultRoute handler={Login}/>
        <Route name="app" handler={App}>
          <DefaultRoute name="dashboard" handler={Board}/>
          <Route name="download" handler={Download} />
          <Route name="build" handler={Build} />
          <Route name="settings" handler={Settings} />
        </Route>
    </Route>
)

var r = 0, run = function () {
    // run the router, i.e. start the app
    Router.run(routes, function (Handler) {
        React.render(<Handler/>, document.getElementById('main'))
    })
}

net.onSessionStateIn(function (data) {
    SessionStateActions.update(data.response)
    location.href = '#/app'
    r || r++ || run()
})

net.onSessionStateOut(function (data) {
    SessionStateActions.destroy()
    location.href = '#/'
    r || r++ || run()
})

net.possibleSessionStateChanged()
