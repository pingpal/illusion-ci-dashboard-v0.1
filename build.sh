PJ=illusion-ci

npm install
gulp build

VERSION=$(date '+%Y-%m-%d-%s')
echo $VERSION > dist/version.txt

mv dist $PJ
mkdir dist
zip -r dist.zip $PJ
cp dist.zip "dist/$PJ-$VERSION.zip"
cp dist.zip "dist/$PJ-latest.zip"

#clean up
rm dist.zip
rm -rf $PJ
