var gulp = require('gulp')
var browserify = require('browserify');
var transform = require('vinyl-transform')
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify')
var reactify = require('reactify')
var gulpif = require('gulp-if')
var rename = require('gulp-rename')
var minifyCss = require('gulp-minify-css')
var uglify = require('gulp-uglify')
var args = require('yargs').argv

var log = function (s) { process.stdout.write(s) }
var t, mark = function () { t = +new Date }
var since = function () { return +new Date -t }

gulp.task('default', function() {

    var prod = args.prod || this.seq.slice(-1)[0] == 'build'

    var bundler = browserify({
        entries: './index.js', transform: 'reactify',
        cache: {}, packageCache: {}, fullPaths: true
    })

    var bundle = function() {
        return bundler
          .bundle().on('error', function (e) { console.log(e.message) })
          .pipe(source('index.js'))
          .pipe(gulpif(prod, buffer()))
          .pipe(gulpif(prod, uglify()))
          .pipe(rename('bundle.js'))
          .pipe(gulp.dest('.'))
    }

    if (args.watch) {
        bundler = watchify(bundler)
        bundler.on('update', function () {
            log('Building... ')
            mark() | bundle()
            log('done in ' + since() + "ms\n")
        })
    }

    return bundle()
})

gulp.task('css', function() {
    return gulp.src('bundle.css')
        .pipe(minifyCss({ keepSpecialComments: 0 }))
        .pipe(gulp.dest('dist'))
})

gulp.task('copy', [ 'default' ], function() {
    return gulp.src([ 'bundle.js', 'index.html' ])
        .pipe(gulp.dest('dist'))
})

gulp.task('build', [ 'css', 'copy' ])
